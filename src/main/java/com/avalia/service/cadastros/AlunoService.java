package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Aluno;
import com.avalia.modelo.repository.AlunoRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class AlunoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected AlunoRepository alunoRepository;

    @Inject
    protected GeralDAO<Aluno> dao;

    @Transacional
    public void salvar(Aluno aluno) {
	dao.salvar(aluno);
    }

    @Transacional
    public void remover(Aluno aluno) {
	dao.remover(aluno);
    }

    public List<Aluno> listarTodos(String nomeAluno) {
	if (!nomeAluno.trim().equals("")) {
	    return alunoRepository.buscarPorDescricao("nome", nomeAluno);
	}
	return alunoRepository.listarTodos();
    }

}
