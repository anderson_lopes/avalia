package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Periodoletivo;
import com.avalia.modelo.repository.PeriodoLetivoRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class PeriodoLetivoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected PeriodoLetivoRepository periodoLetivoRepository;

    @Inject
    protected GeralDAO<Periodoletivo> dao;

    @Transacional
    public void salvar(Periodoletivo periodoLetivo) {
	dao.salvar(periodoLetivo);
    }

    @Transacional
    public void remover(Periodoletivo periodoLetivo) {
	dao.remover(periodoLetivo);
    }

    @Transacional
    public List<Periodoletivo> listarTodos(String nomePeriodoLetivo) {
	if (!nomePeriodoLetivo.trim().equals("")) {
	    return periodoLetivoRepository.buscarPorDescricao("periodoLetivo", nomePeriodoLetivo);
	}
	return periodoLetivoRepository.listarTodos();
    }

}
