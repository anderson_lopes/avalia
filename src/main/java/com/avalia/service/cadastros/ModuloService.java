package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Modulo;
import com.avalia.modelo.repository.ModuloRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class ModuloService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private ModuloRepository moduloRepository;

    @Inject
    protected GeralDAO<Modulo> dao;

    @Transacional
    public void salvar(Modulo modulo) {
	dao.salvar(modulo);
    }

    @Transacional
    public void remover(Modulo modulo) {
	dao.remover(modulo);
    }

    @Transacional
    public List<Modulo> listarTodos(String nomeModulo) {
	if (!nomeModulo.trim().equals("")) {
	    return moduloRepository.buscarPorDescricao("modulo", nomeModulo);
	}
	return moduloRepository.listarTodos();
    }

}
