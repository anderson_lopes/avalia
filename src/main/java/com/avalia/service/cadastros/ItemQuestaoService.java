package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Itemquestao;
import com.avalia.modelo.Questao;
import com.avalia.modelo.repository.ItemQuestaoRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class ItemQuestaoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected ItemQuestaoRepository itemQuestaoRepository;

    @Inject
    protected GeralDAO<Itemquestao> dao;

    @Transacional
    public void salvar(Itemquestao itemQuestao) {
	dao.salvar(itemQuestao);
    }

    @Transacional
    public void remover(Itemquestao itemQuestao) {
	dao.remover(itemQuestao);
    }

    @Transacional
    public List<Itemquestao> listarTodos(String itemQuestao) {
	if (!itemQuestao.trim().equals("")) {
	    return itemQuestaoRepository.buscarPorDescricao("itemquestao", itemQuestao);
	}
	return itemQuestaoRepository.listarTodos();
    }

    @Transacional
    public List<Itemquestao> listarTodos(Questao questao) {
	return itemQuestaoRepository.buscarPorRelacionamento("questao", questao);
    }

}
