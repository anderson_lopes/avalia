package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Funcionario;
import com.avalia.modelo.RelFuncionarioDisciplina;
import com.avalia.modelo.repository.DisciplinaRepository;
import com.avalia.modelo.repository.FuncionarioRepository;
import com.avalia.modelo.repository.RelFuncionarioDisciplinaRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class FuncionarioService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected FuncionarioRepository professorRepository;

    @Inject
    protected DisciplinaRepository disciplinaRepository;

    @Inject
    protected RelFuncionarioDisciplinaRepository relFuncionarioDisciplinaRepository;

    @Inject
    protected GeralDAO<Funcionario> dao;

    @Inject
    protected GeralDAO<RelFuncionarioDisciplina> daoFuncionarioDisciplina;

    @Transacional
    public void salvar(Funcionario professor) {
	dao.salvar(professor);
    }

    @Transacional
    public void salvar(RelFuncionarioDisciplina relFuncionarioDisciplina) {
	daoFuncionarioDisciplina.salvar(relFuncionarioDisciplina);
    }

    @Transacional
    public void remover(Funcionario Professor) {
	dao.remover(Professor);
    }

    @Transacional
    public void remover(RelFuncionarioDisciplina relFuncionarioDisciplina) {
	daoFuncionarioDisciplina.remover(relFuncionarioDisciplina);
    }

    @Transacional
    public List<Funcionario> listarTodos(String nomeProfessor) {
	if (!nomeProfessor.trim().equals("")) {
	    return professorRepository.buscarPorDescricao("funcionario", nomeProfessor);
	}
	return professorRepository.listarTodos();
    }

    @Transacional
    public List<RelFuncionarioDisciplina> listarDisciplinasRelacionadas(Funcionario funcionario) {
	return relFuncionarioDisciplinaRepository.buscarPorRelacionamento("funcionario", funcionario);
    }

    @Transacional
    public List<Disciplina> listarDisciplinas(String query) {
	if (query.trim().equals("")) {
	    return disciplinaRepository.buscarPorDescricao("disciplina", query);
	}
	return disciplinaRepository.listarTodos();
    }

}
