package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Aluno;
import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Periodoletivo;
import com.avalia.modelo.RelAlunoTurma;
import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.RelFuncionarioDisciplina;
import com.avalia.modelo.Tipoturma;
import com.avalia.modelo.Turma;
import com.avalia.modelo.repository.DisciplinaRepository;
import com.avalia.modelo.repository.PeriodoLetivoRepository;
import com.avalia.modelo.repository.RelAlunoTurmaRepository;
import com.avalia.modelo.repository.RelDisciplinaTurmaRepository;
import com.avalia.modelo.repository.RelFuncionarioDisciplinaRepository;
import com.avalia.modelo.repository.TipoTurmaRepository;
import com.avalia.modelo.repository.TurmaRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class TurmaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private TurmaRepository turmaRepository;

    @Inject
    private RelAlunoTurmaRepository relAlunoTurmaRepository;

    @Inject
    private TipoTurmaRepository tipoTurmaRepository;

    @Inject
    private RelDisciplinaTurmaRepository relDisciplinaTurmaRepository;

    @Inject
    protected PeriodoLetivoRepository periodoLetivoRepository;

    @Inject
    protected DisciplinaRepository disciplinaRepository;

    @Inject
    protected RelFuncionarioDisciplinaRepository relFuncionarioDisciplinaRepository;

    @Inject
    private AlunoService AlunoService;

    @Inject
    protected GeralDAO<Turma> dao;

    @Inject
    protected GeralDAO<RelAlunoTurma> daoRelAlunoTurma;

    @Inject
    protected GeralDAO<RelDisciplinaTurma> daoRelDisciplinaTurma;

    @Transacional
    public void salvar(Turma turma) {
	dao.salvar(turma);
    }

    @Transacional
    public void remover(Turma turma) {
	dao.remover(turma);
    }

    @Transacional
    public List<Turma> listarTodos(String nomeTurma) {
	if (!nomeTurma.trim().equals("")) {
	    return turmaRepository.buscarPorDescricao("turma", nomeTurma);
	}
	return turmaRepository.listarTodos();
    }

    @Transacional
    public List<Tipoturma> listarTiposTurma(String nomeTipoTurma) {
	if (!nomeTipoTurma.trim().equals("")) {
	    return tipoTurmaRepository.buscarPorDescricao("tipoturma", nomeTipoTurma);
	}
	return tipoTurmaRepository.listarTodos();
    }

    @Transacional
    public List<Aluno> listarAlunos(String nomeAluno) {
	return AlunoService.listarTodos(nomeAluno);
    }

    @Transacional
    public void salvar(RelAlunoTurma relAlunoTurma) {
	daoRelAlunoTurma.salvar(relAlunoTurma);
    }

    @Transacional
    public void remover(RelAlunoTurma relAlunoTurma) {
	daoRelAlunoTurma.remover(relAlunoTurma);
    }

    @Transacional
    public List<RelAlunoTurma> listarTodosAlunosRelacionados(String nomeAluno, Turma turma) {
	if (nomeAluno != null && !nomeAluno.trim().equals("")) {
	    return relAlunoTurmaRepository.buscarPorDescricao("aluno.nome", nomeAluno, turma);
	}
	return relAlunoTurmaRepository.buscarPorRelacionamento("turma", turma);
    }

    @Transacional
    public List<Periodoletivo> listarPeriodosLetivos(String periodoLetivo) {
	if (!periodoLetivo.trim().equals("")) {
	    return periodoLetivoRepository.buscarPorDescricao("periodoLetivo", periodoLetivo);
	}
	return periodoLetivoRepository.listarTodos();
    }

    @Transacional
    public void salvar(RelDisciplinaTurma relDisciplinaTurma) {
	daoRelDisciplinaTurma.salvar(relDisciplinaTurma);
    }

    @Transacional
    public void remover(RelDisciplinaTurma relDisciplinaTurma) {
	daoRelDisciplinaTurma.remover(relDisciplinaTurma);
    }

    @Transacional
    public List<RelDisciplinaTurma> listarTodasDisciplinasRelacionadas(String nomeDisciplina, Turma turma) {
	if (nomeDisciplina != null && !nomeDisciplina.trim().equals("")) {
	    return relDisciplinaTurmaRepository.buscarPorDescricao("disciplina.disciplina", nomeDisciplina, turma);
	}
	return relDisciplinaTurmaRepository.buscarPorRelacionamento("turma", turma);
    }

    @Transacional
    public List<Disciplina> listarDisciplinas(String disciplina) {
	if (!disciplina.trim().equals("")) {
	    return disciplinaRepository.buscarPorDescricao("disciplina", disciplina);
	}
	return disciplinaRepository.listarTodos();
    }

    @Transacional
    public List<RelFuncionarioDisciplina> listarProfessoresPorDisciplina(Disciplina disciplina) {
	return relFuncionarioDisciplinaRepository.buscarPorDisciplina(disciplina);
    }

}
