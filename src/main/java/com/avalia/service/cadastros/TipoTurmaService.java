package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Tipoturma;
import com.avalia.modelo.repository.TipoTurmaRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class TipoTurmaService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private TipoTurmaRepository tipoTurmaRepository;

    @Inject
    protected GeralDAO<Tipoturma> dao;

    @Transacional
    public void salvar(Tipoturma tipoTurma) {
	dao.salvar(tipoTurma);
    }

    @Transacional
    public void remover(Tipoturma tipoTurma) {
	dao.remover(tipoTurma);
    }

    @Transacional
    public List<Tipoturma> listarTodos(String nomeTipoTurma) {
	if (!nomeTipoTurma.trim().equals("")) {
	    return tipoTurmaRepository.buscarPorDescricao("tipoturma", nomeTipoTurma);
	}
	return tipoTurmaRepository.listarTodos();
    }

}
