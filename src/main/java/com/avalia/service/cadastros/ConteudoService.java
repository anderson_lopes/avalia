package com.avalia.service.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Conteudo;
import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Modulo;
import com.avalia.modelo.Periodoletivo;
import com.avalia.modelo.Tipoturma;
import com.avalia.modelo.repository.ConteudoRepository;
import com.avalia.modelo.repository.DisciplinaRepository;
import com.avalia.modelo.repository.ModuloRepository;
import com.avalia.modelo.repository.PeriodoLetivoRepository;
import com.avalia.modelo.repository.TipoTurmaRepository;
import com.avalia.util.GeralDAO;
import com.avalia.util.Transacional;

public class ConteudoService implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    protected ConteudoRepository conteudoRepository;

    @Inject
    protected DisciplinaRepository disciplinaRepository;

    @Inject
    protected PeriodoLetivoRepository periodoLetivoRepository;

    @Inject
    protected ModuloRepository moduloRepository;

    @Inject
    private TipoTurmaRepository tipoTurmaRepository;

    @Inject
    protected GeralDAO<Conteudo> dao;

    @Transacional
    public void salvar(Conteudo conteudo) {
	dao.salvar(conteudo);
    }

    @Transacional
    public void remover(Conteudo conteudo) {
	dao.remover(conteudo);
    }

    @Transacional
    public List<Conteudo> listarTodos(String conteudo) {
	if (!conteudo.trim().equals("")) {
	    return conteudoRepository.buscarPorDescricao("conteudo", conteudo);
	}
	return conteudoRepository.listarTodos();
    }

    @Transacional
    public List<Disciplina> listarDisciplinas(String disciplina) {
	if (!disciplina.trim().equals("")) {
	    return disciplinaRepository.buscarPorDescricao("disciplina", disciplina);
	}
	return disciplinaRepository.listarTodos();
    }

    @Transacional
    public List<Conteudo> listarTodos(Disciplina disciplina) {
	return conteudoRepository.buscarPorRelacionamento("disciplina", disciplina);
    }

    @Transacional
    public List<Periodoletivo> listarPeriodosLetivos(String periodoLetivo) {
	if (!periodoLetivo.trim().equals("")) {
	    return periodoLetivoRepository.buscarPorDescricao("periodoLetivo", periodoLetivo);
	}
	return periodoLetivoRepository.listarTodos();
    }

    @Transacional
    public List<Modulo> listarModulos(String modulo) {
	if (!modulo.trim().equals("")) {
	    return moduloRepository.buscarPorDescricao("modulo", modulo);
	}
	return moduloRepository.listarTodos();
    }

    @Transacional
    public List<Tipoturma> listarTiposTurma(String nomeTipoTurma) {
	if (!nomeTipoTurma.trim().equals("")) {
	    return tipoTurmaRepository.buscarPorDescricao("tipoturma", nomeTipoTurma);
	}
	return tipoTurmaRepository.listarTodos();
    }

}
