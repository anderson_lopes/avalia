package com.avalia.service.academico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Itemquestao;
import com.avalia.modelo.Modulo;
import com.avalia.modelo.Questao;
import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.Turma;
import com.avalia.modelo.repository.ModuloRepository;
import com.avalia.modelo.repository.QuestaoRepository;
import com.avalia.modelo.repository.RelDisciplinaTurmaRepository;
import com.avalia.modelo.repository.TurmaRepository;
import com.avalia.util.Transacional;

public class AvaliacaoService implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<Questao> questoes;

	@Inject
	private RelDisciplinaTurmaRepository relDisciplinaTurmaRepository;

	@Inject
	private ModuloRepository moduloRepository;

	@Inject
	private TurmaRepository turmaRepository;

	@Inject
	private QuestaoRepository questaoRepository;

	@Transacional
	public List<Turma> listarTurmas() {
		return turmaRepository.listarTodos();
	}

	@Transacional
	public List<RelDisciplinaTurma> listarDisciplinasporTurma(Turma turma) {
		return relDisciplinaTurmaRepository.buscarPorRelacionamento("turma", turma);
	}

	@Transacional
	public List<Modulo> listarModulos(String modulo) {
		if (!modulo.trim().equals("")) {
			return moduloRepository.buscarPorDescricao("modulo", modulo);
		}
		return moduloRepository.listarTodos();
	}

	@Transacional
	public List<Questao> obterQuestoes(Disciplina disciplina, Modulo modulo, int numeroQuestoes) {
		this.questoes = questaoRepository.filtrarQuestoes(disciplina, modulo);
		embaralhaQuestoes();
		validaQuestoes();
		selecionarQuestoes(numeroQuestoes);
		return questoes;
	}

	private void validaQuestoes() {
		List<Questao> questoesValidas = new ArrayList<Questao>();
		for (Questao q : getQuestoes()) {
			q = preparaQuestao(q);
			if (q.getItemquestaos().size() > 1) {
				questoesValidas.add(q);
			}
		}
		this.questoes = questoesValidas;
	}

	private List<Questao> embaralhaQuestoes() {
		if (getQuestoes().size() > 0) {
			Collections.shuffle(getQuestoes());
			for (Questao q : getQuestoes()) {
				Collections.shuffle(q.getItemquestaos());
			}
		}
		return getQuestoes();
	}

	private void selecionarQuestoes(int quantidadeQuestoes) {
		if (getQuestoes().size() > quantidadeQuestoes) {
			List<Questao> questoesfiltradas = new ArrayList<Questao>();
			for (int i = 0; i < quantidadeQuestoes; i++) {
				questoesfiltradas.add(getQuestoes().get(i));
			}
			this.questoes = questoesfiltradas;
		}
	}

	public Questao preparaQuestao(Questao questao) {
		Itemquestao itemCorreto = new Itemquestao();
		List<Itemquestao> questoesErradas = new ArrayList<Itemquestao>();
		for (Itemquestao i : questao.getItemquestaos()) {
			if (i.getCorreto() == true) {
				itemCorreto = i;
			} else if (questoesErradas.size() < 4) {
				questoesErradas.add(i);
			}
		}
		if (itemCorreto.getCorreto() == true && questoesErradas.size() > 0) {
			questao.setItemquestaos(questoesErradas);
			questao.getItemquestaos().add(itemCorreto);
		} else {
			questao.setItemquestaos(null);
		}

		return questao;
	}

	public List<Questao> getQuestoes() {
		if (this.questoes == null) {
			this.questoes = new ArrayList<Questao>();
		}
		return questoes;
	}

}
