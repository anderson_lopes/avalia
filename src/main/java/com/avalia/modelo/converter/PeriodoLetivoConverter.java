package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.Periodoletivo;
import com.avalia.modelo.repository.PeriodoLetivoRepository;

@FacesConverter(forClass = Periodoletivo.class)
public class PeriodoLetivoConverter implements Converter {

    @Inject
    protected PeriodoLetivoRepository anoLetivoRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return anoLetivoRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Periodoletivo) {
	    Periodoletivo anoLetivo = (Periodoletivo) value;
	    return String.valueOf(anoLetivo.getId());
	}
	return "";
    }

}
