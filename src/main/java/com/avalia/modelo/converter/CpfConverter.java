package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "cpfconverter")
public class CpfConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String valor) throws ConverterException {
		if (valor != null && valor != "") {
			valor = valor.toString().replaceAll("[- /.()]", "");
		}
		return valor;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		String cpf = (String) value;
		if (cpf != null && cpf.length() == 11)
			cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "." + cpf.substring(6, 9) + "-"
					+ cpf.substring(9, 11);

		return cpf;
	}
}