package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.RelAvaliacaoQuestao;
import com.avalia.modelo.repository.RelAvaliacaoQuestaoRepository;

@FacesConverter(forClass = RelAvaliacaoQuestao.class)
public class RelAvaliacaoQuestaoConverter implements Converter {

    @Inject
    private RelAvaliacaoQuestaoRepository relAvaliacaoQuestaoRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return relAvaliacaoQuestaoRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof RelAvaliacaoQuestao) {
	    RelAvaliacaoQuestao relAvaliacaoQuestao = (RelAvaliacaoQuestao) value;
	    return String.valueOf(relAvaliacaoQuestao.getId());
	}
	return "";
    }

}
