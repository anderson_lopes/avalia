package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.repository.RelDisciplinaTurmaRepository;

@FacesConverter(forClass = RelDisciplinaTurma.class)
public class RelDisciplinaTurmaConverter implements Converter {

    @Inject
    private RelDisciplinaTurmaRepository relDisciplinaTurmaRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return relDisciplinaTurmaRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof RelDisciplinaTurma) {
	    RelDisciplinaTurma relDisciplinaTurma = (RelDisciplinaTurma) value;
	    return String.valueOf(relDisciplinaTurma.getId());
	}
	return "";
    }

}
