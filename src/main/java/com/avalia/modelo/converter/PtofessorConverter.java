package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.Funcionario;
import com.avalia.modelo.repository.FuncionarioRepository;

@FacesConverter(forClass = Funcionario.class)
public class PtofessorConverter implements Converter {

    @Inject
    private FuncionarioRepository professorRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return professorRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Funcionario) {
	    Funcionario professor = (Funcionario) value;
	    return String.valueOf(professor.getId());
	}
	return "";
    }

}
