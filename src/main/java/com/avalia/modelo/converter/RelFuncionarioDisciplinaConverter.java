package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.RelFuncionarioDisciplina;
import com.avalia.modelo.repository.RelFuncionarioDisciplinaRepository;

@FacesConverter(forClass = RelFuncionarioDisciplina.class)
public class RelFuncionarioDisciplinaConverter implements Converter {

    @Inject
    private RelFuncionarioDisciplinaRepository relFuncionarioDisciplinaRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return relFuncionarioDisciplinaRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof RelFuncionarioDisciplina) {
	    RelFuncionarioDisciplina relFuncionarioDisciplina = (RelFuncionarioDisciplina) value;
	    return String.valueOf(relFuncionarioDisciplina.getId());
	}
	return "";
    }

}
