package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter(value = "foneconverter")
public class FoneConverter implements Converter {
	public Object getAsObject(FacesContext context, UIComponent component, String valor) throws ConverterException {
		if (valor != null && valor != "") {
			valor = valor.toString().replaceAll("[- /.()]", "");
		}
		return valor;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		String fone = (String) value;
		if (fone != null) {
			if (fone.length() == 11) {
				fone = "(" + fone.substring(0, 2) + ") " + fone.substring(2, 7) + "-" + fone.substring(7, 11);
			} else if (fone.length() == 10) {
				fone = "(" + fone.substring(0, 2) + ") " + fone.substring(2, 6) + "-" + fone.substring(7, 10);
			}
		}
		return fone;
	}
}
