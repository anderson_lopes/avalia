package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.Itemquestao;
import com.avalia.modelo.repository.ItemQuestaoRepository;

@FacesConverter(forClass = Itemquestao.class)
public class ItemQuestaoConverter implements Converter {

    @Inject
    private ItemQuestaoRepository itemQuestaoRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return itemQuestaoRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Itemquestao) {
	    Itemquestao itemQuestao = (Itemquestao) value;
	    return String.valueOf(itemQuestao.getId());
	}
	return "";
    }

}
