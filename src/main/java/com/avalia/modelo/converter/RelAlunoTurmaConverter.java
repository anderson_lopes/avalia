package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.RelAlunoTurma;
import com.avalia.modelo.repository.RelAlunoTurmaRepository;

@FacesConverter(forClass = RelAlunoTurma.class)
public class RelAlunoTurmaConverter implements Converter {

    @Inject
    private RelAlunoTurmaRepository relAlunoTurmaRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return relAlunoTurmaRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof RelAlunoTurma) {
	    RelAlunoTurma relAlunoTurma = (RelAlunoTurma) value;
	    return String.valueOf(relAlunoTurma.getId());
	}
	return "";
    }

}
