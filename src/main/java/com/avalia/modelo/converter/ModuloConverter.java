package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.Modulo;
import com.avalia.modelo.repository.ModuloRepository;

@FacesConverter(forClass = Modulo.class)
public class ModuloConverter implements Converter {

    @Inject
    private ModuloRepository moduloRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return moduloRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Modulo) {
	    Modulo modulo = (Modulo) value;
	    return String.valueOf(modulo.getId());
	}
	return "";
    }

}
