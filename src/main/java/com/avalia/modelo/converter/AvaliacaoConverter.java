package com.avalia.modelo.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.avalia.modelo.Aluno;
import com.avalia.modelo.Avaliacao;
import com.avalia.modelo.repository.AvaliacaoRepository;

@FacesConverter(forClass = Avaliacao.class)
public class AvaliacaoConverter implements Converter {

    @Inject
    private AvaliacaoRepository avaliacaoRepository;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
	if (value != null && !value.equals("")) {
	    return avaliacaoRepository.obterPorID(Integer.valueOf(value));
	}
	return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
	if (value instanceof Aluno) {
	    Avaliacao avaliacao = (Avaliacao) value;
	    return String.valueOf(avaliacao.getId());
	}
	return "";
    }

}
