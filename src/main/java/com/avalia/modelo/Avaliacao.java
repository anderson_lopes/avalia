package com.avalia.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQuery(name = "Avaliacao.findAll", query = "SELECT a FROM Avaliacao a")
public class Avaliacao implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Date data;

    @Column(nullable = false)
    private Integer quantidadequestoes;

    @Column(nullable = false)
    private Double nota;

    @ManyToOne
    @JoinColumn(name = "fkdisciplina", nullable = false, columnDefinition = "integer")
    private Disciplina disciplina;

    @ManyToOne
    @JoinColumn(name = "fkprofessor", nullable = false, columnDefinition = "integer")
    private Funcionario funcionario;

    @ManyToOne
    @JoinColumn(name = "fkmodulo", nullable = false, columnDefinition = "integer")
    private Modulo modulo;

    @ManyToOne
    @JoinColumn(name = "fkturma", nullable = false, columnDefinition = "integer")
    private Turma turma;

    @ManyToOne
    @JoinColumn(name = "fkaluno", nullable = false, columnDefinition = "integer")
    private Aluno aluno;

    @OneToMany(mappedBy = "avaliacao")
    private List<RelAvaliacaoQuestao> questoes;

    public Avaliacao() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getData() {
	return this.data;
    }

    public void setData(Date data) {
	this.data = data;
    }

    public Integer getQuantidadequestoes() {
	return this.quantidadequestoes;
    }

    public void setQuantidadequestoes(Integer quantidadequestoes) {
	this.quantidadequestoes = quantidadequestoes;
    }

    public Double getNota() {
	return nota;
    }

    public void setNota(Double nota) {
	this.nota = nota;
    }

    public Disciplina getDisciplina() {
	return this.disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

    public Funcionario getFuncionario() {
	return this.funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
	this.funcionario = funcionario;
    }

    public Modulo getModulo() {
	return this.modulo;
    }

    public void setModulo(Modulo modulo) {
	this.modulo = modulo;
    }

    public Turma getTurma() {
	return this.turma;
    }

    public void setTurma(Turma turma) {
	this.turma = turma;
    }

    public List<RelAvaliacaoQuestao> getQuestoes() {
	return questoes;
    }

    public void setQuestoes(List<RelAvaliacaoQuestao> questoes) {
	this.questoes = questoes;
    }

    public Aluno getAluno() {
	return aluno;
    }

    public void setAluno(Aluno aluno) {
	this.aluno = aluno;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Avaliacao other = (Avaliacao) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}