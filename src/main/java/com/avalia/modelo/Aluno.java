package com.avalia.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

@Entity
@NamedQuery(name = "Aluno.findAll", query = "SELECT a FROM Aluno a")
public class Aluno implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Column(columnDefinition = "text")
    private String celular;

    @CPF(message = "O CPF é inválido!")
    @NotEmpty(message = "O CPF deve ser preenchido.")
    @Column(columnDefinition = "text", nullable = false)
    private String cpf;

    @Past(message = "A data de nascimento deve ser	anterior a data atual.")
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    private Date datanascimento;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String matricula;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String nome;

    @Column(columnDefinition = "text")
    private String responsavel;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "text")
    private String sexo;

    @Column(name = "telefoneresponsavel", columnDefinition = "text")
    private String telefoneResponsavel;

    @OneToMany(mappedBy = "aluno")
    private List<RelAlunoTurma> relAlunoTurmas;

    public Aluno() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getCelular() {
	return this.celular;
    }

    public void setCelular(String celular) {
	this.celular = celular;
    }

    public String getCpf() {
	return this.cpf;
    }

    public void setCpf(String cpf) {
	this.cpf = cpf;
    }

    public Date getDatanascimento() {
	return this.datanascimento;
    }

    public void setDatanascimento(Date datanascimento) {
	this.datanascimento = datanascimento;
    }

    public String getMatricula() {
	return this.matricula;
    }

    public void setMatricula(String matricula) {
	this.matricula = matricula;
    }

    public String getNome() {
	return this.nome;
    }

    public void setNome(String nome) {
	this.nome = nome;
    }

    public String getResponsavel() {
	return this.responsavel;
    }

    public void setResponsavel(String responsavel) {
	this.responsavel = responsavel;
    }

    public String getSexo() {
	return this.sexo;
    }

    public void setSexo(String sexo) {
	this.sexo = sexo;
    }

    public String getTelefoneResponsavel() {
	return this.telefoneResponsavel;
    }

    public void setTelefoneResponsavel(String telefoneResponsavel) {
	this.telefoneResponsavel = telefoneResponsavel;
    }

    public List<RelAlunoTurma> getRelAlunoTurmas() {
	return this.relAlunoTurmas;
    }

    public void setRelAlunoTurmas(List<RelAlunoTurma> relAlunoTurmas) {
	this.relAlunoTurmas = relAlunoTurmas;
    }

    public RelAlunoTurma addRelAlunoTurma(RelAlunoTurma relAlunoTurma) {
	getRelAlunoTurmas().add(relAlunoTurma);
	relAlunoTurma.setAluno(this);

	return relAlunoTurma;
    }

    public RelAlunoTurma removeRelAlunoTurma(RelAlunoTurma relAlunoTurma) {
	getRelAlunoTurmas().remove(relAlunoTurma);
	relAlunoTurma.setAluno(null);

	return relAlunoTurma;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Aluno other = (Aluno) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}