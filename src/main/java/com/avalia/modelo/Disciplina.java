package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Disciplina.findAll", query = "SELECT d FROM Disciplina d")
public class Disciplina implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Column(nullable = false)
    private Integer cargahoraria;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "text")
    private String codmec;

    @NotEmpty
    @Column(nullable = false, columnDefinition = "text")
    private String disciplina;

    @OneToMany(mappedBy = "disciplina")
    private List<Avaliacao> avaliacoes;

    @OneToMany(mappedBy = "disciplina")
    private List<Conteudo> conteudos;

    public Disciplina() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Integer getCargahoraria() {
	return this.cargahoraria;
    }

    public void setCargahoraria(Integer cargahoraria) {
	this.cargahoraria = cargahoraria;
    }

    public String getCodmec() {
	return this.codmec;
    }

    public void setCodmec(String codmec) {
	this.codmec = codmec;
    }

    public String getDisciplina() {
	return this.disciplina;
    }

    public void setDisciplina(String disciplina) {
	this.disciplina = disciplina;
    }

    public List<Avaliacao> getAvaliacoes() {
	return this.avaliacoes;
    }

    public void setAvaliacaos(List<Avaliacao> avaliacoes) {
	this.avaliacoes = avaliacoes;
    }

    public Avaliacao addAvaliacao(Avaliacao avaliacao) {
	avaliacao.setDisciplina(this);
	getAvaliacoes().add(avaliacao);

	return avaliacao;
    }

    public Avaliacao removeAvaliacao(Avaliacao avaliacao) {
	getAvaliacoes().remove(avaliacao);
	avaliacao.setDisciplina(null);

	return avaliacao;
    }

    public List<Conteudo> getConteudos() {
	return this.conteudos;
    }

    public void setConteudos(List<Conteudo> conteudos) {
	this.conteudos = conteudos;
    }

    public Conteudo addConteudo(Conteudo conteudo) {
	getConteudos().add(conteudo);
	conteudo.setDisciplina(this);

	return conteudo;
    }

    public Conteudo removeConteudo(Conteudo conteudo) {
	getConteudos().remove(conteudo);
	conteudo.setDisciplina(null);

	return conteudo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Disciplina other = (Disciplina) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}