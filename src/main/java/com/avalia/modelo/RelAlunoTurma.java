package com.avalia.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "rel_aluno_turma")
@NamedQuery(name = "RelAlunoTurma.findAll", query = "SELECT r FROM RelAlunoTurma r")
public class RelAlunoTurma implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "fkaluno", nullable = false, columnDefinition = "integer")
    private Aluno aluno;

    @ManyToOne
    @JoinColumn(name = "fkturma", nullable = false, columnDefinition = "integer")
    private Turma turma;

    @ManyToOne
    @JoinColumn(name = "fkperiodoletivo", nullable = false, columnDefinition = "integer")
    private Periodoletivo periodoletivo;

    public RelAlunoTurma() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Aluno getAluno() {
	return this.aluno;
    }

    public void setAluno(Aluno aluno) {
	this.aluno = aluno;
    }

    public Turma getTurma() {
	return this.turma;
    }

    public void setTurma(Turma turma) {
	this.turma = turma;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RelAlunoTurma other = (RelAlunoTurma) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

    public Periodoletivo getPeriodoletivo() {
	return periodoletivo;
    }

    public void setPeriodoletivo(Periodoletivo periodoletivo) {
	this.periodoletivo = periodoletivo;
    }

}