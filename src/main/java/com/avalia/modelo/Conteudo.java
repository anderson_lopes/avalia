package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Conteudo.findAll", query = "SELECT c FROM Conteudo c")
public class Conteudo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String conteudo;

    @ManyToOne
    @JoinColumn(name = "fkdisciplina", nullable = false, columnDefinition = "integer")
    private Disciplina disciplina;

    @ManyToOne
    @JoinColumn(name = "fkmodulo", nullable = false, columnDefinition = "integer")
    private Modulo modulo;

    @ManyToOne
    @JoinColumn(name = "fkperiodoletivo", nullable = false, columnDefinition = "integer")
    private Periodoletivo periodoletivo;

    @ManyToOne
    @JoinColumn(name = "fktipoturma", nullable = false, columnDefinition = "integer")
    private Tipoturma tipoturma;

    @OneToMany(mappedBy = "conteudo")
    private List<Questao> questoes;

    public Conteudo() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getConteudo() {
	return this.conteudo;
    }

    public void setConteudo(String conteudo) {
	this.conteudo = conteudo;
    }

    public Disciplina getDisciplina() {
	return this.disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

    public Modulo getModulo() {
	return this.modulo;
    }

    public void setModulo(Modulo modulo) {
	this.modulo = modulo;
    }

    public Periodoletivo getPeriodoletivo() {
	return this.periodoletivo;
    }

    public void setPeriodoletivo(Periodoletivo periodoletivo) {
	this.periodoletivo = periodoletivo;
    }

    public Tipoturma getTipoturma() {
	return tipoturma;
    }

    public void setTipoturma(Tipoturma tipoturma) {
	this.tipoturma = tipoturma;
    }

    public List<Questao> getQuestoes() {
	return this.questoes;
    }

    public void setQuestoes(List<Questao> questoes) {
	this.questoes = questoes;
    }

    public Questao addQuestao(Questao questao) {
	questao.setConteudo(this);
	getQuestoes().add(questao);

	return questao;
    }

    public Questao removeQuestao(Questao questao) {
	getQuestoes().remove(questao);
	questao.setConteudo(null);

	return questao;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Conteudo other = (Conteudo) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}