package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Questao.findAll", query = "SELECT q FROM Questao q")
public class Questao implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Column(nullable = false)
    private Boolean ativo;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String questao;

    @OneToMany(mappedBy = "questao")
    private List<Itemquestao> itemquestaos;

    @ManyToOne
    @JoinColumn(name = "fkconteudo", nullable = false, columnDefinition = "integer")
    private Conteudo conteudo;

    public Questao() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Boolean getAtivo() {
	return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
	this.ativo = ativo;
    }

    public String getQuestao() {
	return this.questao;
    }

    public void setQuestao(String questao) {
	this.questao = questao;
    }

    public List<Itemquestao> getItemquestaos() {
	return this.itemquestaos;
    }

    public void setItemquestaos(List<Itemquestao> itemquestaos) {
	this.itemquestaos = itemquestaos;
    }

    public Itemquestao addItemquestao(Itemquestao itemquestao) {
	getItemquestaos().add(itemquestao);
	itemquestao.setQuestao(this);

	return itemquestao;
    }

    public Itemquestao removeItemquestao(Itemquestao itemquestao) {
	getItemquestaos().remove(itemquestao);
	itemquestao.setQuestao(null);

	return itemquestao;
    }

    public Conteudo getConteudo() {
	return this.conteudo;
    }

    public void setConteudo(Conteudo conteudo) {
	this.conteudo = conteudo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Questao other = (Questao) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}