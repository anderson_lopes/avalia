package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Modulo.findAll", query = "SELECT m FROM Modulo m")
public class Modulo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String modulo;

    @OneToMany(mappedBy = "modulo")
    private List<Avaliacao> avaliacaos;

    @OneToMany(mappedBy = "modulo")
    private List<Conteudo> conteudos;

    public Modulo() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getModulo() {
	return this.modulo;
    }

    public void setModulo(String modulo) {
	this.modulo = modulo;
    }

    public List<Avaliacao> getAvaliacaos() {
	return this.avaliacaos;
    }

    public void setAvaliacaos(List<Avaliacao> avaliacaos) {
	this.avaliacaos = avaliacaos;
    }

    public Avaliacao addAvaliacao(Avaliacao avaliacao) {
	getAvaliacaos().add(avaliacao);
	avaliacao.setModulo(this);

	return avaliacao;
    }

    public Avaliacao removeAvaliacao(Avaliacao avaliacao) {
	getAvaliacaos().remove(avaliacao);
	avaliacao.setModulo(null);

	return avaliacao;
    }

    public List<Conteudo> getConteudos() {
	return this.conteudos;
    }

    public void setConteudos(List<Conteudo> conteudos) {
	this.conteudos = conteudos;
    }

    public Conteudo addConteudo(Conteudo conteudo) {
	getConteudos().add(conteudo);
	conteudo.setModulo(this);

	return conteudo;
    }

    public Conteudo removeConteudo(Conteudo conteudo) {
	getConteudos().remove(conteudo);
	conteudo.setModulo(null);

	return conteudo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Modulo other = (Modulo) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}