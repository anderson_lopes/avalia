package com.avalia.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "rel_disciplina_turma")
@NamedQuery(name = "RelDisciplinaTurma.findAll", query = "SELECT r FROM RelDisciplinaTurma r")
public class RelDisciplinaTurma implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "fkdisciplina", nullable = false, columnDefinition = "integer")
    private Disciplina disciplina;

    @ManyToOne
    @JoinColumn(name = "fkturma", nullable = false, columnDefinition = "integer")
    private Turma turma;

    @ManyToOne
    @JoinColumn(name = "fkperiodoletivo", nullable = false, columnDefinition = "integer")
    private Periodoletivo periodoletivo;

    @ManyToOne
    @JoinColumn(name = "fkrelfuncionariodisciplina", nullable = false, columnDefinition = "integer")
    private RelFuncionarioDisciplina relFuncionarioDisciplina;

    public RelDisciplinaTurma() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Disciplina getDisciplina() {
	return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
	this.disciplina = disciplina;
    }

    public Turma getTurma() {
	return this.turma;
    }

    public void setTurma(Turma turma) {
	this.turma = turma;
    }

    public Periodoletivo getPeriodoletivo() {
	return periodoletivo;
    }

    public void setPeriodoletivo(Periodoletivo periodoletivo) {
	this.periodoletivo = periodoletivo;
    }

    public RelFuncionarioDisciplina getRelFuncionarioDisciplina() {
	return relFuncionarioDisciplina;
    }

    public void setRelFuncionarioDisciplina(RelFuncionarioDisciplina relFuncionarioDisciplina) {
	this.relFuncionarioDisciplina = relFuncionarioDisciplina;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	RelDisciplinaTurma other = (RelDisciplinaTurma) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}