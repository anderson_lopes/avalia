package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.Turma;

public class RelDisciplinaTurmaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public RelDisciplinaTurmaRepository() {

    }

    public RelDisciplinaTurmaRepository(EntityManager manager) {
	this.manager = manager;
    }

    public RelDisciplinaTurma obterPorID(Integer id) {
	return manager.find(RelDisciplinaTurma.class, id);
    }

    public List<RelDisciplinaTurma> listarTodos() {
	return manager.createQuery("from RelDisciplinaTurma order by id desc", RelDisciplinaTurma.class)
		.getResultList();
    }

    public List<RelDisciplinaTurma> buscarPorDescricao(String campo, String value, Turma turma) {
	return manager
		.createQuery(
			"from RelDisciplinaTurma where " + campo
				+ " like :value and  turma = :turma order by disciplina.disciplina",
			RelDisciplinaTurma.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").setParameter("turma", turma).getResultList();
    }

    public List<RelDisciplinaTurma> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from RelDisciplinaTurma where " + campo + " = :value order by turma.turma",
		RelDisciplinaTurma.class).setParameter("value", value).getResultList();
    }

}
