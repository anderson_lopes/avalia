package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Modulo;
import com.avalia.modelo.Questao;

public class QuestaoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Questao obterPorID(Integer id) {
	return manager.find(Questao.class, id);
    }

    public List<Questao> listarTodos() {
	return manager.createQuery("from Questao order by questao", Questao.class).getResultList();
    }

    public List<Questao> buscarPorDescricao(String campo, String value) {
	return manager.createQuery("from Questao where " + campo + " like :value ", Questao.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Questao> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Questao where " + campo + " = :value ", Questao.class)
		.setParameter("value", value).getResultList();
    }

    public List<Questao> filtrarQuestoes(Disciplina disciplina, Modulo modulo) {
	return manager
		.createQuery("from Questao where conteudo.disciplina =  :disciplina and conteudo.modulo = :modulo",
			Questao.class)
		.setParameter("disciplina", disciplina).setParameter("modulo", modulo).getResultList();
    }
}
