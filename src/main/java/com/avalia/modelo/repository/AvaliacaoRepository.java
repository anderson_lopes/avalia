package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Avaliacao;

public class AvaliacaoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public AvaliacaoRepository() {

    }

    public AvaliacaoRepository(EntityManager manager) {
	this.manager = manager;
    }

    public Avaliacao obterPorID(Integer id) {
	return manager.find(Avaliacao.class, id);
    }

    public List<Avaliacao> listarTodos() {
	return manager.createQuery("from Avaliacao order by id desc", Avaliacao.class).getResultList();
    }

    public List<Avaliacao> buscarPorDescricao(String campo, String value) {
	return manager
		.createQuery("from Avaliacao where " + campo + " like :value order by disciplina", Avaliacao.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Avaliacao> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Avaliacao where " + campo + " = :value order by disciplina", Avaliacao.class)
		.setParameter("value", value).getResultList();
    }

}
