package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.RelAvaliacaoQuestao;

public class RelAvaliacaoQuestaoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public RelAvaliacaoQuestaoRepository() {

    }

    public RelAvaliacaoQuestaoRepository(EntityManager manager) {
	this.manager = manager;
    }

    public RelAvaliacaoQuestao obterPorID(Integer id) {
	return manager.find(RelAvaliacaoQuestao.class, id);
    }

    public List<RelAvaliacaoQuestao> listarTodos() {
	return manager.createQuery("from RelAvaliacaoQuestao order by id desc", RelAvaliacaoQuestao.class)
		.getResultList();
    }

    public List<RelAvaliacaoQuestao> buscarPorDescricao(String campo, String value) {
	return manager
		.createQuery("from RelAvaliacaoQuestao where " + campo + " like :value order by avaliacao",
			RelAvaliacaoQuestao.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<RelAvaliacaoQuestao> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from RelAvaliacaoQuestao where " + campo + " = :value order by avaliacao",
		RelAvaliacaoQuestao.class).setParameter("value", value).getResultList();
    }

}
