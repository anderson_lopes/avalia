package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Itemquestao;

public class ItemQuestaoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Itemquestao obterPorID(Integer id) {
	return manager.find(Itemquestao.class, id);
    }

    public List<Itemquestao> listarTodos() {
	return manager.createQuery("from Itemquestao order by itemquestao", Itemquestao.class).getResultList();
    }

    public List<Itemquestao> buscarPorDescricao(String campo, String value) {
	return manager
		.createQuery("from Itemquestao where " + campo + " like :value order by itemquestao", Itemquestao.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Itemquestao> buscarPorRelacionamento(String campo, Object value) {
	return manager
		.createQuery("from Itemquestao where " + campo + " = :value order by itemquestao", Itemquestao.class)
		.setParameter("value", value).getResultList();
    }

}
