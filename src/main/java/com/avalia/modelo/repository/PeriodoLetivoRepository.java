package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Periodoletivo;

public class PeriodoLetivoRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Periodoletivo obterPorID(Integer pk) {
	return manager.find(Periodoletivo.class, pk);
    }

    public List<Periodoletivo> listarTodos() {
	return manager.createQuery("from Periodoletivo order by periodoLetivo desc", Periodoletivo.class)
		.getResultList();
    }

    public List<Periodoletivo> buscarPorDescricao(String campo, String value) {
	return manager.createQuery("from Periodoletivo where " + campo + " like :value order by periodoLetivo desc",
		Periodoletivo.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Periodoletivo> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Periodoletivo where " + campo + " = :value order by periodoLetivo desc",
		Periodoletivo.class).setParameter("value", value).getResultList();
    }

}
