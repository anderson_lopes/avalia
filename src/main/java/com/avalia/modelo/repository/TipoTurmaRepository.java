package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Tipoturma;

public class TipoTurmaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public Tipoturma obterPorID(Integer pktipoturma) {
	return manager.find(Tipoturma.class, pktipoturma);
    }

    public List<Tipoturma> listarTodos() {
	return manager.createQuery("from Tipoturma order by tipoturma", Tipoturma.class).getResultList();
    }

    public List<Tipoturma> buscarPorDescricao(String campo, String value) {
	return manager.createQuery("from Tipoturma where " + campo + " like :value order by tipoturma", Tipoturma.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Tipoturma> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Tipoturma where " + campo + " = :value order by tipoturma", Tipoturma.class)
		.setParameter("value", value).getResultList();
    }

}
