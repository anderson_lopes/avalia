package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Disciplina;
import com.avalia.modelo.RelFuncionarioDisciplina;

public class RelFuncionarioDisciplinaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public RelFuncionarioDisciplinaRepository() {

    }

    public RelFuncionarioDisciplinaRepository(EntityManager manager) {
	this.manager = manager;
    }

    public RelFuncionarioDisciplina obterPorID(Integer id) {
	return manager.find(RelFuncionarioDisciplina.class, id);
    }

    public List<RelFuncionarioDisciplina> listarTodos() {
	return manager.createQuery("from RelFuncionarioDisciplina order by id desc", RelFuncionarioDisciplina.class)
		.getResultList();
    }

    public List<RelFuncionarioDisciplina> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery(
		"from RelFuncionarioDisciplina where " + campo + " = :value order by funcionario.funcionario",
		RelFuncionarioDisciplina.class).setParameter("value", value).getResultList();
    }

    public List<RelFuncionarioDisciplina> buscarPorDisciplina(Disciplina disciplina) {
	return manager.createQuery(
		"from RelFuncionarioDisciplina where disciplina	 = :disciplina and ativo is true order by funcionario.funcionario",
		RelFuncionarioDisciplina.class).setParameter("disciplina", disciplina).getResultList();
    }

}
