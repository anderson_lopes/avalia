package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Modulo;

public class ModuloRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public ModuloRepository() {

    }

    public ModuloRepository(EntityManager manager) {
	this.manager = manager;
    }

    public Modulo obterPorID(Integer id) {
	return manager.find(Modulo.class, id);
    }

    public List<Modulo> listarTodos() {
	return manager.createQuery("from Modulo order by id desc", Modulo.class).getResultList();
    }

    public List<Modulo> buscarPorDescricao(String campo, String value) {
	return manager.createQuery("from Modulo where " + campo + " like :value order by modulo", Modulo.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Modulo> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Modulo where " + campo + " = :value order by modulo", Modulo.class)
		.setParameter("value", value).getResultList();
    }

}
