package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.Funcionario;

public class FuncionarioRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public FuncionarioRepository() {

    }

    public FuncionarioRepository(EntityManager manager) {
	this.manager = manager;
    }

    public Funcionario obterPorID(Integer pk) {
	return manager.find(Funcionario.class, pk);
    }

    public List<Funcionario> listarTodos() {
	return manager.createQuery("from Funcionario order by id desc", Funcionario.class).getResultList();
    }

    public List<Funcionario> buscarPorDescricao(String campo, String value) {
	return manager.createQuery("from Funcionario f where " + campo + " like :value order by f.funcionario",
		Funcionario.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
    }

    public List<Funcionario> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from Funcionario f where " + campo + " = :value order by p.funcionario",
		Funcionario.class).setParameter("value", value).getResultList();
    }

}
