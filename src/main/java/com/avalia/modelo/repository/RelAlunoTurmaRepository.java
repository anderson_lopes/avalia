package com.avalia.modelo.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.avalia.modelo.RelAlunoTurma;
import com.avalia.modelo.Turma;

public class RelAlunoTurmaRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private EntityManager manager;

    public RelAlunoTurmaRepository() {

    }

    public RelAlunoTurmaRepository(EntityManager manager) {
	this.manager = manager;
    }

    public RelAlunoTurma obterPorID(Integer id) {
	return manager.find(RelAlunoTurma.class, id);
    }

    public List<RelAlunoTurma> listarTodos() {
	return manager.createQuery("from RelAlunoTurma order by id desc", RelAlunoTurma.class).getResultList();
    }

    public List<RelAlunoTurma> buscarPorDescricao(String campo, String value, Turma turma) {
	return manager
		.createQuery(
			"from RelAlunoTurma where " + campo + " like :value and  turma = :turma order by aluno.nome",
			RelAlunoTurma.class)
		.setParameter("value", "%" + value.toUpperCase() + "%").setParameter("turma", turma).getResultList();
    }

    public List<RelAlunoTurma> buscarPorRelacionamento(String campo, Object value) {
	return manager.createQuery("from RelAlunoTurma where " + campo + " = :value order by turma.turma",
		RelAlunoTurma.class).setParameter("value", value).getResultList();
    }

}
