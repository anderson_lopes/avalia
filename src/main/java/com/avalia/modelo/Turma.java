package com.avalia.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Turma.findAll", query = "SELECT t FROM Turma t")
public class Turma implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Temporal(TemporalType.TIME)
    @Column(nullable = false)
    private Date horafinal;

    @Temporal(TemporalType.TIME)
    @Column(nullable = false)
    private Date horaInicial;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String turma;

    @OneToMany(mappedBy = "turma")
    private List<Avaliacao> avaliacaos;

    @OneToMany(mappedBy = "turma")
    private List<RelAlunoTurma> relAlunoTurmas;

    @ManyToOne
    @JoinColumn(name = "fktipoturma", nullable = false, columnDefinition = "integer")
    private Tipoturma tipoturma;

    public Turma() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getHorafinal() {
	return this.horafinal;
    }

    public void setHorafinal(Date horafinal) {
	this.horafinal = horafinal;
    }

    public Date getHoraInicial() {
	return this.horaInicial;
    }

    public void setHoraInicial(Date horaInicial) {
	this.horaInicial = horaInicial;
    }

    public String getTurma() {
	return this.turma;
    }

    public void setTurma(String turma) {
	this.turma = turma;
    }

    public List<Avaliacao> getAvaliacaos() {
	return this.avaliacaos;
    }

    public void setAvaliacaos(List<Avaliacao> avaliacaos) {
	this.avaliacaos = avaliacaos;
    }

    public Avaliacao addAvaliacao(Avaliacao avaliacao) {
	getAvaliacaos().add(avaliacao);
	avaliacao.setTurma(this);

	return avaliacao;
    }

    public Avaliacao removeAvaliacao(Avaliacao avaliacao) {
	getAvaliacaos().remove(avaliacao);
	avaliacao.setTurma(null);

	return avaliacao;
    }

    public List<RelAlunoTurma> getRelAlunoTurmas() {
	return this.relAlunoTurmas;
    }

    public void setRelAlunoTurmas(List<RelAlunoTurma> relAlunoTurmas) {
	this.relAlunoTurmas = relAlunoTurmas;
    }

    public RelAlunoTurma addRelAlunoTurma(RelAlunoTurma relAlunoTurma) {
	getRelAlunoTurmas().add(relAlunoTurma);
	relAlunoTurma.setTurma(this);

	return relAlunoTurma;
    }

    public RelAlunoTurma removeRelAlunoTurma(RelAlunoTurma relAlunoTurma) {
	getRelAlunoTurmas().remove(relAlunoTurma);
	relAlunoTurma.setTurma(null);

	return relAlunoTurma;
    }

    public Tipoturma getTipoturma() {
	return this.tipoturma;
    }

    public void setTipoturma(Tipoturma tipoturma) {
	this.tipoturma = tipoturma;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Turma other = (Turma) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}