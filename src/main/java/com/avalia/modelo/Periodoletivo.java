package com.avalia.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Periodoletivo.findAll", query = "SELECT p FROM Periodoletivo p")
public class Periodoletivo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datafim;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datainicio;

    @Column(nullable = false)
    private Integer dias;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String periodoletivo;

    @OneToMany(mappedBy = "periodoletivo")
    private List<Conteudo> conteudos;

    public Periodoletivo() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Date getDatafim() {
	return this.datafim;
    }

    public void setDatafim(Date datafim) {
	this.datafim = datafim;
    }

    public Date getDatainicio() {
	return this.datainicio;
    }

    public void setDatainicio(Date datainicio) {
	this.datainicio = datainicio;
    }

    public Integer getDias() {
	return this.dias;
    }

    public void setDias(Integer dias) {
	this.dias = dias;
    }

    public String getPeriodoletivo() {
	return this.periodoletivo;
    }

    public void setPeriodoletivo(String periodoletivo) {
	this.periodoletivo = periodoletivo;
    }

    public List<Conteudo> getConteudos() {
	return this.conteudos;
    }

    public void setConteudos(List<Conteudo> conteudos) {
	this.conteudos = conteudos;
    }

    public Conteudo addConteudo(Conteudo conteudo) {
	getConteudos().add(conteudo);
	conteudo.setPeriodoletivo(this);

	return conteudo;
    }

    public Conteudo removeConteudo(Conteudo conteudo) {
	getConteudos().remove(conteudo);
	conteudo.setPeriodoletivo(null);

	return conteudo;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Periodoletivo other = (Periodoletivo) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}