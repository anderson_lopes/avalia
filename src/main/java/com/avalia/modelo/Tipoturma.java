package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Tipoturma.findAll", query = "SELECT t FROM Tipoturma t")
public class Tipoturma implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String codtipoturma;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String tipoturma;

    @OneToMany(mappedBy = "tipoturma")
    private List<Turma> turmas;

    public Tipoturma() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getCodtipoturma() {
	return this.codtipoturma;
    }

    public void setCodtipoturma(String codtipoturma) {
	this.codtipoturma = codtipoturma;
    }

    public String getTipoturma() {
	return this.tipoturma;
    }

    public void setTipoturma(String tipoturma) {
	this.tipoturma = tipoturma;
    }

    public List<Turma> getTurmas() {
	return this.turmas;
    }

    public void setTurmas(List<Turma> turmas) {
	this.turmas = turmas;
    }

    public Turma addTurma(Turma turma) {
	getTurmas().add(turma);
	turma.setTipoturma(this);

	return turma;
    }

    public Turma removeTurma(Turma turma) {
	getTurmas().remove(turma);
	turma.setTipoturma(null);

	return turma;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Tipoturma other = (Tipoturma) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}