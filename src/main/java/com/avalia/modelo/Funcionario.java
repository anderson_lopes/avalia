package com.avalia.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CPF;

@Entity
@NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
public class Funcionario implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @CPF(message = "O CPF é inválido!")
    @NotEmpty(message = "O CPF deve ser preenchido.")
    @Column(columnDefinition = "text", nullable = false)
    private String cpf;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String funcionario;

    @OneToMany(mappedBy = "funcionario")
    private List<Avaliacao> avaliacoes;

    public Funcionario() {
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public String getCpf() {
	return this.cpf;
    }

    public void setCpf(String cpf) {
	this.cpf = cpf;
    }

    public String getFuncionario() {
	return this.funcionario;
    }

    public void setFuncionario(String funcionario) {
	this.funcionario = funcionario;
    }

    public List<Avaliacao> getAvaliacoes() {
	return this.avaliacoes;
    }

    public void setAvaliacoes(List<Avaliacao> avaliacoes) {
	this.avaliacoes = avaliacoes;
    }

    public Avaliacao addAvaliacao(Avaliacao avaliacao) {
	avaliacao.setFuncionario(this);
	getAvaliacoes().add(avaliacao);

	return avaliacao;
    }

    public Avaliacao removeAvaliacao(Avaliacao avaliacao) {
	getAvaliacoes().remove(avaliacao);
	avaliacao.setFuncionario(null);

	return avaliacao;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Funcionario other = (Funcionario) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}