package com.avalia.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@NamedQuery(name = "Itemquestao.findAll", query = "SELECT i FROM Itemquestao i")
public class Itemquestao implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Integer id;

    @Column(nullable = false)
    private Boolean ativo;

    @Column(nullable = false)
    private Boolean correto;

    @NotEmpty
    @Column(columnDefinition = "text", nullable = false)
    private String itemquestao;

    @ManyToOne
    @JoinColumn(name = "fkquestao", nullable = false, columnDefinition = "integer")
    private Questao questao;

    @Transient
    private Boolean selecionado;

    public Itemquestao() {
	this.selecionado = false;
    }

    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Boolean getAtivo() {
	return this.ativo;
    }

    public void setAtivo(Boolean ativo) {
	this.ativo = ativo;
    }

    public Boolean getCorreto() {
	return this.correto;
    }

    public void setCorreto(Boolean correto) {
	this.correto = correto;
    }

    public String getItemquestao() {
	return this.itemquestao;
    }

    public void setItemquestao(String itemquestao) {
	this.itemquestao = itemquestao;
    }

    public Questao getQuestao() {
	return this.questao;
    }

    public void setQuestao(Questao questao) {
	this.questao = questao;
    }

    public Boolean getSelecionado() {
	return selecionado;
    }

    public void setSelecionado(Boolean selecionado) {
	this.selecionado = selecionado;
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((id == null) ? 0 : id.hashCode());
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Itemquestao other = (Itemquestao) obj;
	if (id == null) {
	    if (other.id != null)
		return false;
	} else if (!id.equals(other.id))
	    return false;
	return true;
    }

}