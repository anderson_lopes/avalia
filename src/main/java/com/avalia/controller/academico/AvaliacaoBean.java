package com.avalia.controller.academico;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.avalia.modelo.Avaliacao;
import com.avalia.modelo.Itemquestao;
import com.avalia.modelo.Modulo;
import com.avalia.modelo.Questao;
import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.Turma;
import com.avalia.service.academico.AvaliacaoService;
import com.avalia.util.FacesUtils;

@Named
@ViewScoped
public class AvaliacaoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Avaliacao avaliacao;

    private int numeroQuestoes = 5;

    private Double nota = 0D;

    private Itemquestao item;

    private List<Questao> questoesDaAvaliacaoGerada;

    @Inject
    private AvaliacaoService avaliacaoService;

    @Inject
    protected FacesUtils facesUtils;

    public void iniciar() {
	novaAvaliacao();
    }

    public void novaAvaliacao() {
	this.avaliacao = new Avaliacao();
    }

    public void gerarAvaliacao() {
	try {
	    this.questoesDaAvaliacaoGerada = null;
	    this.questoesDaAvaliacaoGerada = avaliacaoService.obterQuestoes(getAvaliacao().getDisciplina(),
		    getAvaliacao().getModulo(), getNumeroQuestoes());
	} catch (Exception e) {
	    facesUtils.exibeErro("Avaliação não pode ser gerada!", null);
	}
    }

    public void selecionarItem(Questao questao) {
	if (getItem() != null) {
	    for (Itemquestao i : questao.getItemquestaos()) {
		if (i.getId().equals(getItem().getId())) {
		    i.setSelecionado(true);
		} else {
		    i.setSelecionado(false);
		}
	    }
	}
    }

    public void finalizarAvaliacao() {
	this.nota = 0D;
	for (Questao q : questoesDaAvaliacaoGerada) {
	    if (acertou(q)) {
		this.nota = (this.nota + 10 / getNumeroQuestoes());
	    }
	}
	facesUtils.exibeSucesso("Avaliação concluída!", null);
    }

    public boolean acertou(Questao q) {
	for (Itemquestao i : q.getItemquestaos()) {
	    if (i.getSelecionado() != null) {
		if (i.getSelecionado() == true && i.getCorreto() == true) {
		    return true;
		}
	    }
	}
	return false;
    }

    public Avaliacao getAvaliacao() {
	return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
	this.avaliacao = avaliacao;
    }

    public List<Turma> listarTurmas() {
	return avaliacaoService.listarTurmas();
    }

    public List<RelDisciplinaTurma> listarDisciplinasPorTurma(String query) {
	return avaliacaoService.listarDisciplinasporTurma(this.getAvaliacao().getTurma());
    }

    public List<Modulo> listarModulos(String query) {
	return avaliacaoService.listarModulos(query);
    }

    public List<Questao> getQuestoesDaAvaliacaoGerada() {
	return questoesDaAvaliacaoGerada;
    }

    public int getNumeroQuestoes() {
	return numeroQuestoes;
    }

    public void setNumeroQuestoes(int numeroQuestoes) {
	this.numeroQuestoes = numeroQuestoes;
    }

    public Itemquestao getItem() {
	return item;
    }

    public void setItem(Itemquestao item) {
	this.item = item;
    }

    public Double getNota() {
	return this.nota;
    }

}
