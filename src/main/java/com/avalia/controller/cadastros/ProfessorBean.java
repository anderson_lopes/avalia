package com.avalia.controller.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Funcionario;
import com.avalia.modelo.RelFuncionarioDisciplina;
import com.avalia.service.cadastros.FuncionarioService;
import com.avalia.util.FacesUtils;

@Named
@ViewScoped
public class ProfessorBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Funcionario professor;

    private RelFuncionarioDisciplina relProfessorDisciplina;

    private String nomeProfessor;

    private String nomeDisciplina;

    private List<Funcionario> professores;

    private List<RelFuncionarioDisciplina> professorDisciplinas;

    @Inject
    protected FuncionarioService professorService;

    @Inject
    protected FacesUtils facesUtils;

    public void iniciar() {
	this.setNomeProfessor("");
	this.setNomeDisciplina("");
	pesquisar();
    }

    public void novo() {
	this.professor = new Funcionario();
    }

    public void vincularDisciplina() {
	this.relProfessorDisciplina = new RelFuncionarioDisciplina();
	this.relProfessorDisciplina.setFuncionario(getProfessor());
    }

    public void salvar() {
	try {
	    professorService.salvar(this.professor);
	    novo();
	    pesquisar();
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void salvarProfessorDisciplina() {
	try {
	    professorService.salvar(this.relProfessorDisciplina);
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limparVinculo();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void remover() {
	try {
	    professorService.remover(this.professor);
	    pesquisar();
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void removerProfessorDisciplina() {
	try {
	    professorService.remover(this.relProfessorDisciplina);
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limparVinculo();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void limpar() {
	this.professor = null;
	limparVinculo();
    }

    public void limparVinculo() {
	this.relProfessorDisciplina = null;
    }

    public void pesquisar() {
	this.getProfessores();
    }

    public void pesquisarVinculos() {
	this.getProfessores();
    }

    public Funcionario getProfessor() {
	return professor;
    }

    public void setProfessor(Funcionario professor) {
	this.professor = professor;
    }

    public String getNomeProfessor() {
	return nomeProfessor;
    }

    public void setNomeProfessor(String nomeProfessor) {
	this.nomeProfessor = nomeProfessor;
    }

    public List<Funcionario> getProfessores() {
	this.professores = professorService.listarTodos(getNomeProfessor());
	return professores;
    }

    public List<Disciplina> listarDisciplinas(String query) {
	return professorService.listarDisciplinas(query);
    }

    public RelFuncionarioDisciplina getRelProfessorDisciplina() {
	return relProfessorDisciplina;
    }

    public void setRelProfessorDisciplina(RelFuncionarioDisciplina relProfessorDisciplina) {
	this.relProfessorDisciplina = relProfessorDisciplina;
    }

    public List<RelFuncionarioDisciplina> getProfessorDisciplinas() {
	this.professorDisciplinas = professorService.listarDisciplinasRelacionadas(getProfessor());
	return professorDisciplinas;
    }

    public String getNomeDisciplina() {
	return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
	this.nomeDisciplina = nomeDisciplina;
    }

}
