package com.avalia.controller.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.avalia.modelo.Periodoletivo;
import com.avalia.service.cadastros.PeriodoLetivoService;
import com.avalia.util.FacesUtils;

@Named
@ViewScoped
public class PeriodoLetivoBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Periodoletivo periodoLetivo;

    private String nomePeriodoLetivo;

    private List<Periodoletivo> periodosLetivos;

    @Inject
    protected PeriodoLetivoService periodoLetivoService;

    @Inject
    protected FacesUtils facesUtils;

    public void iniciar() {
	this.setNomePeriodoLetivo("");
	pesquisar();
    }

    public void novo() {
	this.periodoLetivo = new Periodoletivo();
    }

    public void salvar() {
	try {
	    periodoLetivoService.salvar(this.periodoLetivo);
	    this.periodoLetivo = new Periodoletivo();
	    this.pesquisar();
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    this.periodoLetivo = null;
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void remover() {
	try {
	    periodoLetivoService.remover(this.periodoLetivo);
	    this.pesquisar();
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    this.periodoLetivo = null;
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void limpar() {
	this.periodoLetivo = null;
    }

    public void pesquisar() {
	this.getPeriodosLetivos();
    }

    public Periodoletivo getPeriodoLetivo() {
	return periodoLetivo;
    }

    public void setPeriodoLetivo(Periodoletivo periodoLetivo) {
	this.periodoLetivo = periodoLetivo;
    }

    public String getNomePeriodoLetivo() {
	return nomePeriodoLetivo;
    }

    public void setNomePeriodoLetivo(String nomePeriodoLetivo) {
	this.nomePeriodoLetivo = nomePeriodoLetivo;
    }

    public List<Periodoletivo> getPeriodosLetivos() {
	this.periodosLetivos = periodoLetivoService.listarTodos(getNomePeriodoLetivo());
	return periodosLetivos;
    }

}
