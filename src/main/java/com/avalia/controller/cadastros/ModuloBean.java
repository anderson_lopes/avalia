package com.avalia.controller.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.avalia.modelo.Modulo;
import com.avalia.service.cadastros.ModuloService;
import com.avalia.util.FacesUtils;

@Named
@ViewScoped
public class ModuloBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Modulo modulo;

    private String nomeModulo;

    private List<Modulo> modulos;

    @Inject
    protected ModuloService moduloService;

    @Inject
    protected FacesUtils facesUtils;

    public void iniciar() {
	this.setNomeModulo("");
	pesquisar();
    }

    public void novo() {
	this.modulo = new Modulo();
    }

    public void salvar() {
	try {
	    moduloService.salvar(this.modulo);
	    novo();
	    pesquisar();
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void remover() {
	try {
	    moduloService.remover(this.modulo);
	    pesquisar();
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void limpar() {
	this.modulo = null;
    }

    public void pesquisar() {
	this.getModulos();
    }

    public Modulo getModulo() {
	return modulo;
    }

    public void setModulo(Modulo modulo) {
	this.modulo = modulo;
    }

    public String getNomeModulo() {
	return nomeModulo;
    }

    public void setNomeModulo(String nomeModulo) {
	this.nomeModulo = nomeModulo;
    }

    public List<Modulo> getModulos() {
	this.modulos = moduloService.listarTodos(getNomeModulo());
	return modulos;
    }

}
