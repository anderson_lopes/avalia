package com.avalia.controller.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.avalia.modelo.Aluno;
import com.avalia.modelo.Disciplina;
import com.avalia.modelo.Periodoletivo;
import com.avalia.modelo.RelAlunoTurma;
import com.avalia.modelo.RelDisciplinaTurma;
import com.avalia.modelo.RelFuncionarioDisciplina;
import com.avalia.modelo.Tipoturma;
import com.avalia.modelo.Turma;
import com.avalia.service.cadastros.TurmaService;
import com.avalia.util.FacesUtils;

@Named
@ViewScoped
public class TurmaBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private Turma turma;

    private RelAlunoTurma relAlunoTurma;

    private RelDisciplinaTurma relDisciplinaTurma;

    private String nomeTurma;

    private String nomeAluno;

    private String nomeDisciplina;

    private List<Turma> turmas;

    private List<RelAlunoTurma> alunosVinculados;

    private List<RelDisciplinaTurma> disciplinasVinculadas;

    @Inject
    protected TurmaService turmaRN;

    @Inject
    protected FacesUtils facesUtils;

    public void iniciar() {
	this.setNomeTurma("");
	pesquisar();
    }

    public void novo() {
	this.turma = new Turma();
    }

    public void vincularAluno() {
	this.relAlunoTurma = new RelAlunoTurma();
    }

    public void vincularDisciplina() {
	this.relDisciplinaTurma = new RelDisciplinaTurma();
    }

    public void salvar() {
	try {
	    turmaRN.salvar(this.turma);
	    novo();
	    pesquisar();
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void salvarVinculoAluno() {
	try {
	    this.relAlunoTurma.setTurma(getTurma());
	    turmaRN.salvar(getRelAlunoTurma());
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limparVinculos();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void salvarVinculoDisciplina() {
	try {
	    getRelDisciplinaTurma().setTurma(getTurma());
	    turmaRN.salvar(getRelDisciplinaTurma());
	    facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
	    limparVinculos();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível inserir o registro!", null);
	}
    }

    public void remover() {
	try {
	    turmaRN.remover(this.turma);
	    this.pesquisar();
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limpar();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void excluirVinculoAlunoTurma(RelAlunoTurma row) {
	try {
	    turmaRN.remover(row);
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limparVinculos();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}

    }

    public void excluirVinculoDisciplinaTurma(RelDisciplinaTurma row) {
	try {
	    turmaRN.remover(row);
	    facesUtils.exibeSucesso("Registro removido com sucesso!", null);
	    limparVinculos();
	} catch (Exception e) {
	    facesUtils.exibeErro("não foi possível remover o registro!", null);
	}
    }

    public void limpar() {
	this.turma = null;
	limparVinculos();
    }

    public void limparVinculos() {
	this.relAlunoTurma = null;
	this.relDisciplinaTurma = null;
    }

    public void pesquisar() {
	this.getTurmas();
    }

    public Turma getTurma() {
	return turma;
    }

    public void setTurma(Turma turma) {
	this.turma = turma;
    }

    public String getNomeTurma() {
	return nomeTurma;
    }

    public void setNomeTurma(String nomeTurma) {
	this.nomeTurma = nomeTurma;
    }

    public List<Turma> getTurmas() {
	this.turmas = turmaRN.listarTodos(getNomeTurma());
	return turmas;
    }

    public List<Tipoturma> listarTiposTurma(String query) {
	return turmaRN.listarTiposTurma(query);
    }

    public List<Aluno> listaAlunos(String query) {
	return turmaRN.listarAlunos(query);
    }

    public RelAlunoTurma getRelAlunoTurma() {
	return relAlunoTurma;
    }

    public void setRelAlunoTurma(RelAlunoTurma relAlunoTurma) {
	this.relAlunoTurma = relAlunoTurma;
    }

    public List<RelAlunoTurma> getAlunosVinculados() {
	alunosVinculados = turmaRN.listarTodosAlunosRelacionados(getNomeAluno(), getTurma());
	return alunosVinculados;
    }

    public List<RelDisciplinaTurma> getDisciplinasVinculadas() {
	disciplinasVinculadas = turmaRN.listarTodasDisciplinasRelacionadas(getNomeDisciplina(), getTurma());
	return disciplinasVinculadas;
    }

    public List<Periodoletivo> listarPeriodosLetivos(String query) {
	return turmaRN.listarPeriodosLetivos(query);
    }

    public List<RelFuncionarioDisciplina> listarProfessoresPorDisciplina(String query) {
	return turmaRN.listarProfessoresPorDisciplina(this.relDisciplinaTurma.getDisciplina());
    }

    public String getNomeAluno() {
	return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
	this.nomeAluno = nomeAluno;
    }

    public RelDisciplinaTurma getRelDisciplinaTurma() {
	return relDisciplinaTurma;
    }

    public void setRelDisciplinaTurma(RelDisciplinaTurma relDisciplinaTurma) {
	this.relDisciplinaTurma = relDisciplinaTurma;
    }

    public List<Disciplina> listarDisciplinas(String query) {
	return turmaRN.listarDisciplinas(query);
    }

    public String getNomeDisciplina() {
	return nomeDisciplina;
    }

    public void setNomeDisciplina(String nomeDisciplina) {
	this.nomeDisciplina = nomeDisciplina;
    }

}
