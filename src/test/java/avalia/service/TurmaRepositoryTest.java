package avalia.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.avalia.modelo.Turma;
import com.avalia.modelo.repository.TurmaRepository;

import avalia.util.DbUnitHelper;

public class TurmaRepositoryTest {
	private static DbUnitHelper dbUnitHelper;
	private static EntityManagerFactory factory;

	private EntityManager manager;
	private TurmaRepository repository;

	@BeforeClass
	public static void initClass() {
		dbUnitHelper = new DbUnitHelper("DbUnitXml");
		factory = Persistence.createEntityManagerFactory("avaliatest");
	}

	@Before
	public void init() {
		dbUnitHelper.execute(DatabaseOperation.DELETE_ALL, "turma.xml");

		dbUnitHelper.execute(DatabaseOperation.INSERT, "turma.xml");

		manager = factory.createEntityManager();
		this.repository = new TurmaRepository(manager);
	}

	@After
	public void end() {
		this.manager.close();
	}

	@Test
	public void deveRetornarDuasTurmas() {
		List<Turma> resultado = repository.listarTodos();
		Assert.assertEquals("resultado", 2, resultado.size());
	}

	@Test
	public void deveRetornarPrimeiraSerie() {
		List<Turma> resultado = repository.buscarPorDescricao("turma", "1ª SERIE");
		Assert.assertEquals("resultado", 1, resultado.size());
	}

	@Test
	public void deveRetornarPorID() {
		Turma turma = new Turma();
		turma.setId(1);
		Assert.assertEquals("resultado", turma, repository.obterPorID(1));
	}

}
