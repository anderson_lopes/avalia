package avalia.service;

import java.util.Locale;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.avalia.controller.AutenticacaoBean;

public class AutenticacaoTest {

	private AutenticacaoBean autenticacao;

	@Before
	public void inicio() {
		this.autenticacao = new AutenticacaoBean();
	}

	@Test
	public void deveRetornarURLAluno() {
		autenticacao.setUsuario("ALUNO");
		autenticacao.setSenha("aluno");
		Assert.assertEquals("resultado", "/paginas/principal?faces-redirect=true", autenticacao.autenticar());
	}

	@Test
	public void deveRetornarURLAdmin() {
		autenticacao.setUsuario("ADMIN");
		autenticacao.setSenha("admin");
		Assert.assertEquals("resultado", "/paginas/principal?faces-redirect=true", autenticacao.autenticar());
	}

	@Test
	public void deveRetornarURLProfessor() {
		autenticacao.setUsuario("PROFESSOR");
		autenticacao.setSenha("professor");
		Assert.assertEquals("resultado", "/paginas/principal?faces-redirect=true", autenticacao.autenticar());
	}

	@Test
	public void obterLocalePortuguesBrasil() {
		Assert.assertFalse("Idioma porrtugues", autenticacao.getLocale().equals(Locale.getDefault().toString()));
	}

	@Test(expected = NullPointerException.class)
	public void deveRetornarPaginaLogin() {
		autenticacao.setUsuario("ALUNO");
		autenticacao.setSenha("sdfsfdsfdsf");
		autenticacao.autenticar();
	}

	@Test
	public void obterEncodingISO() {
		Assert.assertFalse("Encoding iso-8859-1",
				autenticacao.getEncoding().equals(System.getProperties().get("file.encoding")));
	}

}
