package avalia.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AutenticacaoTest.class, DisciplinaRepositoryTest.class, TurmaRepositoryTest.class })
public class AllTests {

}
